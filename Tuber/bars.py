from tqdm import tqdm
from time import sleep


for i in tqdm(range(10), bar_format='{l_bar}{bar:20}{r_bar}', desc='Installing happiness;)'):
    sleep(1)
