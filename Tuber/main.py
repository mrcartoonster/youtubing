from pathlib import Path
from typing import List

from pytube import YouTube
from rich import print
from tqdm import tqdm

vids = Path('/Users/evanbaird/Projects/Personal_Projects/YouTubes/Tuber/Videos')


def downloading_video(video_link: str):
    """
    First function to work out downloading.
    """


    yt = YouTube(video_link)
    # Learn how to login!
    # yt = YouTube(video_link, allow_oauth_cache=True, use_oauth=True)

    dl = yt.streams.get_highest_resolution()

    file = dl.download(output_path=video_link)


videos = [
        'https://www.youtube.com/watch?v=Gsk4g1-ioDk',
        'https://www.youtube.com/watch?v=WfHnF66L-Os',
        'https://www.youtube.com/watch?v=9btjsByT3I8',
        'https://www.youtube.com/watch?v=HsEwsO523uo',
        'https://www.youtube.com/watch?v=jMkqmPqJqEU',
        ]

def tasks(seq: List):
    for _ in tqdm(seq, desc='Downloading Youtube vids', smoothing=0.8):
        downloading_video(_)


def main(videos: List):
    tasks(videos)


if __name__ == "__main__":
    main(videos)
