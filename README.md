# YouTube Download CLI

We're going to learn how to make a YouTube downloader.

## First command(s)

Create a command that saves the YouTube videos to the `Videos` directory.

## Clone the PyTube CLI

Once you have that running, try and clone the [PyTube's CLI](https://pytube.io/en/latest/user/cli.html)
